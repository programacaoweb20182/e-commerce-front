import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';


const appRoutes: Routes = [
    { path: '', redirectTo: '/produto/home', pathMatch: 'full'},
    { path: 'produto', loadChildren: './dominio/produto/produto.module#ProdutoModule'},
    { path: 'categoria', loadChildren: './dominio/categoria/categoria.module#CategoriaModule'},
    { path: 'cor', loadChildren: './dominio/cor/cor.module#CorModule'},
    { path: 'colecao', loadChildren: './dominio/colecao/colecao.module#ColecaoModule'},
    { path: 'carrinho', loadChildren: './dominio/carrinho/carrinho.module#CarrinhoModule'},
    { path: 'contato', loadChildren: './dominio/contato/contato.module#ContatoModule'}
];



@NgModule({
    imports: [RouterModule.forRoot(
        appRoutes,
        { enableTracing: false }
    )],
    exports: [RouterModule]
  })

  export class AppRouting {}

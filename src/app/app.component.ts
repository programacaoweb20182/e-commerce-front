import { Component, OnInit } from '@angular/core';
import { CategoriaService } from './dominio/categoria/categoria.service';
import { Router } from '@angular/router';
import { Categoria } from './dominio/categoria/categoria';
import { CarrinhoService } from './dominio/carrinho/carrinho.service';
import { Carrinho } from './dominio/carrinho/carrinho';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ECommerce-front';

  categorias: Categoria[];

  constructor(
    private categoriaService: CategoriaService,
    private router: Router
  ) {}

  ngOnInit() {
    this.categoriaService.buscarTodos()
    .subscribe(resposta => {
      this.categorias = resposta; });
  }
}

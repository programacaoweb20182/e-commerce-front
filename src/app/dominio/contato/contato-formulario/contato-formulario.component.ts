import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { Contato } from '../contato';
import { ContatoService } from '../contato.service';

@Component({
  selector: 'app-contato-lista',
  templateUrl: './contato-formulario.component.html',
  styleUrls: ['./contato-formulario.component.css']
})
export class ContatoFormularioComponent implements OnInit {

  contato: Contato;
  contatoForm: FormGroup;
  titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private contatoService: ContatoService
  ) { }

  ngOnInit() {

    this.contato = new Contato();

    /* Obter o `ID` passado por parâmetro na URL */
    this.contato.id = this.route.snapshot.params['id'];

    /* Reactive Forms */
    this.contatoForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      email: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      mensagem: this.builder.control('', [Validators.required, Validators.maxLength(50)])
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.contato.id != null) {
      this.contatoService.buscarPeloId(this.contato.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.contatoForm.patchValue(retorno);

        });
    }

  }

  salvar(contato: Contato) {
    console.log(contato);
    if (this.contatoForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.contatoService.salvar(contato)
        .subscribe(response => {
            console.log('Curso salvo com sucesso');

            // retorna para a lista
            this.router.navigate(['/contato']);
            window.location.reload();
          },
          (error) => {
            console.log('Erro no back-end');
          });
    }
  }

}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { ContatoListaComponent } from './contato-lista/contato-lista.component';
import { ContatoFormularioComponent } from './contato-formulario/contato-formulario.component';

const contatoRoutes: Routes = [
  { path: '', component: ContatoListaComponent},
  { path: 'visualizar/:id', component: ContatoFormularioComponent},
  { path: 'novo', component: ContatoFormularioComponent},
  { path: 'alterar/:id', component: ContatoFormularioComponent},
];


@NgModule({
  imports: [RouterModule.forChild(contatoRoutes)],
  exports: [RouterModule]
})

export class ContatoRouting {}

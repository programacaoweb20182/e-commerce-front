import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { Contato } from './contato';

@Injectable()
export class ContatoService {

    private URL = 'http://localhost:8888';
    constructor(private http: HttpClient) { }

    buscarTodos(): Observable<Contato[]> {
        return this.http
            .get<Contato[]>(`${this.URL}/contato`);
    }

    buscarPeloId(id: number): Observable<Contato> {
        return this.http
            .get<Contato>(`${this.URL}/contato/${id}`)
            .pipe(
                map(response => response)
            );
    }

  compareFn( optionOne, optionTwo ): boolean {
    return optionOne.id === optionTwo.id;
  }

    salvar(contato: Contato): Observable<Contato> {

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
            })
        };

        if (contato.id) {
            return this.http
                .put<Contato>(`${this.URL}/contato`, JSON.stringify(contato), httpOptions );
        } else {
            return this.http
                .post<Contato>(`${this.URL}/contato`, JSON.stringify(contato), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${this.URL}/contato/${id}`);
    }

}

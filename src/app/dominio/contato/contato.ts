export class Contato {
  id: number;
  nome: string;
  email: string;
  mensagem: string;
}

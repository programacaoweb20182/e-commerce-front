import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { ContatoRouting } from './contato.routing';
import { ContatoService } from './contato.service';
import { ContatoListaComponent } from './contato-lista/contato-lista.component';
import { ContatoFormularioComponent } from './contato-formulario/contato-formulario.component';


@NgModule({
  declarations: [
    ContatoListaComponent,
    ContatoFormularioComponent
  ],
  imports: [
    // Angular
    HttpModule,
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,

    // Componente
    ContatoRouting
  ],
  providers: [
    // Serviços
    ContatoService
  ]
})

export class ContatoModule { }

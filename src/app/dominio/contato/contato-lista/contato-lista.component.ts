import { Component, OnInit } from '@angular/core';

import { Contato } from '../contato';
import { ContatoService } from '../../contato/contato.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contato-lista',
  templateUrl: './contato-lista.component.html',
  styleUrls: ['./contato-lista.component.css']
})
export class ContatoListaComponent implements OnInit {

  contatos: Contato[];


  constructor(
    private contatoService: ContatoService,
    private router: Router
  ) {}

  ngOnInit() {

    this.contatoService.buscarTodos()
      .subscribe(resposta => {
        this.contatos = resposta; });
  }

  excluir(contatoId: number) {
    this.contatoService.excluir(contatoId)
      .subscribe(resposta => {
        console.log('Contato excluído com sucesso');
        // retorna para a lista
        this.router.navigate(['/contato']);
        window.location.reload();
      } );
  }

}

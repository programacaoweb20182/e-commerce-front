import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { Cor } from '../cor';
import { CorService } from '../cor.service';

@Component({
  selector: 'app-cor-lista',
  templateUrl: './cor-formulario.component.html',
  styleUrls: ['./cor-formulario.component.css']
})
export class CorFormularioComponent implements OnInit {

  cor: Cor;
  corForm: FormGroup;
  titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private corService: CorService
  ) { }

  ngOnInit() {

    this.cor = new Cor();

    /* Obter o `ID` passado por parâmetro na URL */
    this.cor.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.cor.id == null)
      ? 'Novo Cor'
      : 'Alterar Cor';

    /* Reactive Forms */
    this.corForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)])
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.cor.id != null) {
      this.corService.buscarPeloId(this.cor.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.corForm.patchValue(retorno);

        });
    }

  }

  salvar(cor: Cor) {
    console.log(cor);
    if (this.corForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.corService.salvar(cor)
        .subscribe(response => {
            console.log('Curso salvo com sucesso');

            // retorna para a lista
            this.router.navigate(['/cor']);
            window.location.reload();
          },
          (error) => {
            console.log('Erro no back-end');
          });
    }
  }

}

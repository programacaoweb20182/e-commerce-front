import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { Cor } from './cor';

@Injectable()
export class CorService {

    private URL = 'http://localhost:8888';
    constructor(private http: HttpClient) { }

    buscarTodos(): Observable<Cor[]> {
        return this.http
            .get<Cor[]>(`${this.URL}/cor`);
    }

    buscarPeloId(id: number): Observable<Cor> {
        return this.http
            .get<Cor>(`${this.URL}/cor/${id}`)
            .pipe(
                map(response => response)
            );
    }

  compareFn( optionOne, optionTwo ): boolean {
    return optionOne.id === optionTwo.id;
  }

    salvar(cor: Cor): Observable<Cor> {

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
            })
        };

        if (cor.id) {
            return this.http
                .put<Cor>(`${this.URL}/cor`, JSON.stringify(cor), httpOptions );
        } else {
            return this.http
                .post<Cor>(`${this.URL}/cor`, JSON.stringify(cor), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${this.URL}/cor/${id}`);
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { CorRouting } from './cor.routing';
import { CorService } from './cor.service';
import { CorListaComponent } from './cor-lista/cor-lista.component';
import { CorFormularioComponent } from './cor-formulario/cor-formulario.component';


@NgModule({
  declarations: [
    CorListaComponent,
    CorFormularioComponent,
  ],
  imports: [
    // Angular
    HttpModule,
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,

    // Componente
    CorRouting
  ],
  providers: [
    // Serviços
    CorService
  ]
})

export class CorModule { }

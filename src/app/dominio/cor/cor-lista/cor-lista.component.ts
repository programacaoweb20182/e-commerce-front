import { Component, OnInit } from '@angular/core';

import { Cor } from '../cor';
import { CorService } from '../../cor/cor.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cor-lista',
  templateUrl: './cor-lista.component.html',
  styleUrls: ['./cor-lista.component.css']
})
export class CorListaComponent implements OnInit {

  cors: Cor[];


  constructor(
    private corService: CorService,
    private router: Router
  ) {}

  ngOnInit() {

    this.corService.buscarTodos()
      .subscribe(resposta => {
        this.cors = resposta; });
  }

  excluir(corId: number) {
    this.corService.excluir(corId)
      .subscribe(resposta => {
        console.log('Cor excluído com sucesso');
        // retorna para a lista
        this.router.navigate(['/cor']);
        window.location.reload();
      } );
  }

}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { CorListaComponent } from './cor-lista/cor-lista.component';
import { CorFormularioComponent } from './cor-formulario/cor-formulario.component';

const corRoutes: Routes = [
  { path: '', component: CorListaComponent},
  { path: 'visualizar/:id', component: CorFormularioComponent},
  { path: 'novo', component: CorFormularioComponent},
  { path: 'alterar/:id', component: CorFormularioComponent},
];


@NgModule({
  imports: [RouterModule.forChild(corRoutes)],
  exports: [RouterModule]
})

export class CorRouting {}

import { CarrinhoRouting } from "./carrinho.routing";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { HttpModule } from "@angular/http";
import { CarrinhoListaComponent } from "./carrinho-lista/carrinho-lista.component";
import { NgModule } from "@angular/core";
import { CarrinhoService } from "./carrinho.service";
import { CorService } from "../cor/cor.service";

@NgModule({
  declarations: [CarrinhoListaComponent],
  imports: [
    // Angular
    HttpModule,
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,

    // Component
    CarrinhoRouting
  ],
  providers: [
    // Serviços
    CarrinhoService,
    CorService
  ]
})
export class CarrinhoModule {}

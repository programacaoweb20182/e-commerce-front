import { Produto } from "../produto/produto";
import { Cor } from "../cor/cor";

export class ItemCarrinho {

    constructor(
        public produto: Produto,
        public quantidade: number,
        public cor: Cor
    ) {
    }

}

import { Produto } from "../produto/produto";
import { Cor } from "../cor/cor";
export class Carrinho {
  public produtos: Produto[] = new Array<Produto>();
  public qtdProduto: number = 0;
  public cor: Cor;
}

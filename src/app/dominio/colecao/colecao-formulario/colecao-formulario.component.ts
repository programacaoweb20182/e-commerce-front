import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { Colecao } from '../colecao';
import { ColecaoService } from '../colecao.service';

@Component({
  selector: 'app-colecao-lista',
  templateUrl: './colecao-formulario.component.html',
  styleUrls: ['./colecao-formulario.component.css']
})
export class ColecaoFormularioComponent implements OnInit {

  colecao: Colecao;
  colecaoForm: FormGroup;
  titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private colecaoService: ColecaoService
  ) { }

  ngOnInit() {

    this.colecao = new Colecao();

    /* Obter o `ID` passado por parâmetro na URL */
    this.colecao.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.colecao.id == null)
      ? 'Novo Coleção'
      : 'Alterar Coleção';

    /* Reactive Forms */
    this.colecaoForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)])
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.colecao.id != null) {
      this.colecaoService.buscarPeloId(this.colecao.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.colecaoForm.patchValue(retorno);

        });
    }

  }

  salvar(colecao: Colecao) {
    console.log(colecao);
    if (this.colecaoForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.colecaoService.salvar(colecao)
        .subscribe(response => {
            console.log('Curso salvo com sucesso');

            // retorna para a lista
            this.router.navigate(['/colecao']);
            window.location.reload();
          },
          (error) => {
            console.log('Erro no back-end');
          });
    }
  }

}

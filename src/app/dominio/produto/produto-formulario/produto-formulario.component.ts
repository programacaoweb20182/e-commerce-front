import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { Produto } from '../produto';
import { ProdutoService } from '../produto.service';
import {CategoriaService} from '../../categoria/categoria.service';
import {Categoria} from '../../categoria/categoria';
import { ColecaoService } from '../../colecao/colecao.service';
import { Colecao } from '../../colecao/colecao';


@Component({
  selector: 'app-produto-lista',
  templateUrl: './produto-formulario.component.html',
  styleUrls: ['./produto-formulario.component.css']
})
export class ProdutoFormularioComponent implements OnInit {

  produto: Produto;
  categorias: Categoria[];
  colecoes:  Colecao[];
  produtoForm: FormGroup;
  titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private produtoService: ProdutoService,
    private categoriaService: CategoriaService,
    private colecaoService: ColecaoService
  ) { }

  ngOnInit() {

    this.produto = new Produto();

    /* Obter o `ID` passado por parâmetro na URL */
    this.produto.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.produto.id == null)
    ? 'Novo Produto'
    : 'Alterar Produto';

    this.categoriaService.buscarTodos().subscribe(resposta => {
      this.categorias = resposta; });

      this.colecaoService.buscarTodos().subscribe(resposta => {
        this.colecoes = resposta; });


    /* Reactive Forms */
    this.produtoForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      categoria: this.builder.control('', [Validators.required]),
      colecao: this.builder.control('', [Validators.required]),
      descricao: this.builder.control('', [Validators.required]),
      preco: this.builder.control('', [Validators.required]),
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.produto.id != null) {
      this.produtoService.buscarPeloId(this.produto.id)
        .subscribe(retorno => {

          // Atualiza o formulário com os valores retornados
          this.produtoForm.patchValue(retorno);

        });
    }

  }

  salvar(produto: Produto) {
    console.log(produto);
    if (this.produtoForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.produtoService.salvar(produto)
      .subscribe(response => {
        console.log('Curso salvo com sucesso');

        // retorna para a lista
        this.router.navigate(['/produto']);
      },
      (error) => {
        console.log('Erro no back-end');
      });
    }
  }

}

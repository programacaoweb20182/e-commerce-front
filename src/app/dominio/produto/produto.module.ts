import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { ProdutoRouting } from './produto.routing';
import { ProdutoService } from './produto.service';
import { ProdutoListaComponent } from './produto-lista/produto-lista.component';
import { ProdutoFormularioComponent } from './produto-formulario/produto-formulario.component';
import { ProdutoHomeComponent } from './produto-home/produto-home.component';
import { ColecaoService } from '../colecao/colecao.service';
import { CorService } from '../cor/cor.service';

@NgModule({
    declarations: [
        ProdutoListaComponent,
        ProdutoFormularioComponent,
        ProdutoHomeComponent
    ],
    imports: [
        // Angular
        HttpModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        ProdutoRouting
    ],
    providers: [
        // Serviços
        ProdutoService,
        ColecaoService,
        CorService
    ]
})

export class ProdutoModule { }

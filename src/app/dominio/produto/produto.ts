import {Categoria} from '../categoria/categoria';
import {Colecao} from '../colecao/colecao';

export class Produto {
  id: number;
  nome: string;
  categoria: Categoria;
  colecao: Colecao;
  descricao: string;
  preco: number;
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


import { Produto } from '../produto';
import { ProdutoService } from '../produto.service';
import { CarrinhoService } from '../../carrinho/carrinho.service';
import { Cor } from '../../cor/cor';
import { CorService } from '../../cor/cor.service';
import { IfStmt } from '@angular/compiler';
import { CategoriaService } from '../../categoria/categoria.service';
import { Categoria } from '../../categoria/categoria';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'produto-lista',
  templateUrl: './produto-home.component.html',
  styleUrls: ['./produto-home.component.css']
})
export class ProdutoHomeComponent implements OnInit {

  produtos: Produto[];
  cores: Cor[];
  cor: Cor;
  categorias: Categoria[];
  categoria: Categoria;


  constructor(
    private produtoService: ProdutoService,
    private carrinhoService: CarrinhoService,
    private corService: CorService,
    private categoriaService: CategoriaService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.produtoService.buscarTodos()
      .subscribe(resposta => {
        this.produtos = resposta;
      });

    this.corService.buscarTodos()
      .subscribe(resposta => {
        this.cores = resposta;
      });

    this.categoriaService.buscarTodos().subscribe(resposta => {
      this.categorias = resposta;
    });

  }

  selectCor(event: any) {
    this.corService.buscarPeloId(event.target.value).subscribe(resposta => {
      this.cor = resposta;
    });
  }

  selectCategoria(event: any) {
    this.categoriaService.buscarPeloId(event.target.value).subscribe(resposta => {
      this.categoria = resposta;
    });
  }

  adicionarProduto(produto: Produto) {
    if (this.cor) {
      this.carrinhoService.inserirCarrinho(produto, this.cor);
    } else {
      console.log('Cor invalida')
    }
  }

  filtrarCat() {
    if (this.categoria.id != null) {
      this.produtoService.buscarTodosCategoria(this.categoria.id)
        .subscribe(resposta => {
          this.produtos = resposta;
        });
    } else {
      this.produtoService.buscarTodos()
        .subscribe(resposta => {
          this.produtos = resposta;
        });
    }
  }

  removerCat(){
    this.produtoService.buscarTodos()
        .subscribe(resposta => {
          this.produtos = resposta;
        });
  }

}
